<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


		
	
	public function legal(){
		$admin_info = $this->master->getRecords('admin');
	
		$data = array(
			'middle_content'=>'manage-legals',
			'title'=>'Legal',
			'admin_info'=>$admin_info,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	
	public function legal2(){
		$admin_info = $this->master->getRecords('admin');
	
		$data = array(
			'middle_content'=>'manage-legals2',
			'title'=>'Common Term &amp; Conditions',
			'admin_info'=>$admin_info,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	public function bannertext(){
		
		$thefield = 'banner_text';
		$admin_info = $this->master->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Banner Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function pagetext(){
		
		$thefield = 'light_section';
		$admin_info = $this->master->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Page Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
		

	
	public function homebutton(){
		
		$thefield = 'buy_button_show';
		$admin_info = $this->master->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contentbutton',
			'title'=>'Home Buy Button',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}		
	
	public function buy_button_show(){
		$checked = $_POST['checked'];
		
		$arr = array('buy_button_show'=>$checked);
		
		$this->master->updateRecord('admin', $arr, array('id'=>'2'));
		echo json_encode($arr);	
	
	}

}