
    <section class="bg-primary no-padding-bottom">
        <div class="container">
            <div class="col-xs-12">
                <h2 class="section-heading text-center">Sign Up</h2>
                <hr class="light"/>
                <p class="text-center">Already have an account? <a href="<?php echo base_url().'login'?>" class="btn btn-default btn-sm">Login</a></p>
            </div>
        </div>
    </section>
    <section class="login_section">

        <div class="container">
            
        
            <div class="omb_login">
                <div class="row">
                    <div class="col-xs-12">
                    
                    </div>
                </div>
                <div class="row omb_row-sm-offset-3 omb_socialButtons">
                    <div class="col-xs-6 col-sm-3">
                        <a href="#" class="btn btn-lg btn-block omb_btn-facebook">
                            <i class="fa fa-facebook visible-xs"></i>
                            <span class="hidden-xs">Facebook</span>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <a href="#" class="btn btn-lg btn-block omb_btn-google">
                            <i class="fa fa-google-plus visible-xs"></i>
                            <span class="hidden-xs">Google+</span>
                        </a>
                    </div>	
                </div>
        
                <div class="row omb_row-sm-offset-3 omb_loginOr">
                    <div class="col-xs-12 col-sm-6">
                        <hr class="omb_hrOr">
                        <span class="omb_spanOr">or</span>
                    </div>
                </div>
        
                <div class="row omb_row-sm-offset-3">
                    <div class="col-xs-12 col-sm-6">	
                        <form class="omb_loginForm" id="signup_form">
                            <input type="hidden" class="form-control" name="google_id">
                            <input type="hidden" class="form-control" name="facebook_id">
                        	<div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                    <input type="text" class="form-control" name="name" placeholder="Studio Name">
                                </div>
                            </div>
          
                        	<div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                </div>
                            </div>
          
                        	<div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                            </div>

                        	<div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
                            </div>
        
                        </form>
                    </div>
                </div>
            </div>
        
        
        
        </div>

        
    </section>
