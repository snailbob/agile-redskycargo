<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content text-center">In the event of a claim</h3>
         <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <p>In the event of loss or damage to your goods you should immediately  notify the agent shown on your <strong>insurance certificate</strong>. This agent is our  correspondent and will know exactly how to help you. The agent will inspect your  goods to determine the cause of loss or damage, calculate the value of the  damage plus remaining cargo, and survey the extent of the damage. </p>
               <p>You will need to provide the agent with documentation including insurance  certificate, the original invoice, bill of lading and repair invoice if  applicable. </p>
               <p><strong>Important</strong>:  DO NOT under any circumstances destroy or dispose of damaged goods until the agent  has inspected the goods or confirmed to you that they are no longer required.</p>
            </div>
         </div>
      </div>
   </div>
</div>
