<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content text-center">About RedSky</h3>
         <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <p><strong>&nbsp;</strong></p>
               <p align="center"><em>Leading  the way in marine cargo insurance RedSky provides fast, reliable, single  transit cover for consumers, small to medium enterprises and large  organisations.</em></p>
               <div> </div>
               <p>&nbsp;</p>
               <p><strong>The name RedSky comes from the old  phrase:</strong></p>
               <p><em>Red Sky at night, sailors delight.</em><br />
                  <em>Red Sky at morning, sailors take  warning.</em>
               </p>
               <p><strong>&nbsp;</strong></p>
               <p><strong>As keen sailors</strong>,  this phrase resonated with the founders of RedSky. We know how quickly storms  can develop and how to plan and prepare for those weather changes.</p>
               <p><strong>As passionate Cargo Insurers</strong>, we know how important it is to have  the right insurance and the right Insurer standing alongside you when your  goods are lost or damaged.</p>
               <p><em>At RedSky we have  a clear goal in mind: to make purchasing insurance for goods in transit fast,  reliable and cost effective.   </em></p>
               <p>Few cargo insurers  out there can offer the ease of purchasing single transit cover like RedSky –  and no one else can match the seamless simplicity that our online environment  provides. </p>
               <p>Purchasing cargo  insurance for goods in transit should be easy and cost effective for everyone –  with RedSky it is.</p>
               <p>But don&rsquo;t just take  our word for it. RedSky is backed 100% by Syndicates at Lloyds, with a network  that covers over 170 countries and found in every major port and commercial  centre in the world. </p>
               <p>&nbsp;</p>
               <p><strong>Whether you&rsquo;re partnering with us, or lucky enough to be  sailing with us, you can rest assured you&rsquo;re in safe hands.</strong></p>
            </div>
         </div>
         <div class="row" style="margin-top: 50px;">
            <div class="col-sm-4 col-sm-offset-2 text-center">
               <a href="javascript:;" class="btn btn-red buy_transit_btn">GET QUOTE</a>
            </div>
            <div class="col-sm-4 text-center">
               <a href="<?php echo base_url().'contactus'?>" class="btn btn-red">Contact Us</a>
            </div>
         </div>
      </div>
   </div>
</div>
