
<!doctype html>
<html class="no-js" lang="">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="RedSky Cargo is the simpler, smarter, seamless delivery service putting the logic back into logistics.">
        <meta name="keywords" content="ecommerce,RedSky Cargo,Logistics,Mover,Deliverer,courier,delivery,delivering difficult items,premium delivery">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url().'assets/meemeep/'?>static/JES1LRTWnFUqFXF3iKywOQ38t020F4TTBBzKXrxxkjv.ico" type="image/x-icon">

            <title>RedSky Cargo</title>

        <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">



        <link href="<?php echo base_url().'assets/meemeep/'?>static/feyTmWO0bMeD1aEaHxHMNNeKNRUGPPPfS1U6H5Eg00z.css" type="text/css" rel="stylesheet" media="screen, projection" />



         <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
         <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
         <link rel="stylesheet" href="<?php echo base_url().'assets/meemeep/'?>lib/bootstrap/bootstrap.min.css">
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.6/jquery.fullPage.min.css">




          <!-- Plugin CSS -->
          <?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

          <!-- Plugin CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">


          <link href="<?php echo base_url().'assets/meemeep/'?>static/eqAMgtx1ZdhVAZ7XHESdPFDzIRASWGjS7BiEOGnGhSb.css" type="text/css" rel="stylesheet" media="screen, projection" />
          <link href="<?php echo base_url().'assets/meemeep/'?>static/custom.css" type="text/css" rel="stylesheet" media="screen, projection" />


  		<script>
  			var base_url = '<?php echo base_url() ?>';
  			var uri_1 = '<?php echo $this->uri->segment(1) ?>';
  			var uri_2 = '<?php echo $this->uri->segment(2) ?>';
  			var uri_3 = '<?php echo $this->uri->segment(3) ?>';
  			var uri_4 = '<?php echo $this->uri->segment(4) ?>';
  			var user_id = '<?php echo $this->session->userdata('id') ?>';
  			var user_name = '<?php echo $this->session->userdata('name') ?>';
  			var user_location = '<?php echo $this->session->userdata('location') ?>';
  			var user_address = '<?php echo $this->session->userdata('address') ?>';
  			var user_type = '<?php echo $this->session->userdata('type') ?>';
  			var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
  			var customer_type = '<?php echo $this->session->userdata('customer_type') ?>';
  		</script>
      <script src="<?php echo base_url().'assets/meemeep/'?>static/knffveD2OzCF16hdyrEAfPWa4B3ZjiSrq7fkcGVU1VX.js"></script>

	    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	    <!-------------------------------->
	    <!-- link to liveAgentStyle.css -->
	    <!-------------------------------->
	    <link rel="stylesheet" href="<?php echo base_url().'assets/meemeep/'?>static/O6H3OVWClXGKVT6552GXx18epTs9syatcA3WgDWEnuS.css" type="text/css">
</head>
<body>

  <div class="header-push"></div>
  <header class="generic-header">
      <div class="container content-side-padding clearfix">
          <div class="col">
              <a href="<?php echo base_url() ?>" id="index" class="logo-2"><img src="<?php echo base_url().'assets/meemeep/static/qPqvynPHJHWaFgvoN9BYekMooIPlMUooSeTLkzowpUd.png'?>" alt="Logo" width="auto" height="50"></a>
          </div>
          <div class="col-r clearfix">
              <div class="col">
                  <ul class="generic-menu-top clearfix">
                    <?php if($this->session->userdata('id') == '') { ?>
                      <li><a href="#" data-toggle="modal" data-target="#loginModal" style="padding: 28px">Login</a></li>
                    <?php } else {?>
                      <li><a href="<?php echo base_url().'dashboard' ?>" style="padding: 28px"><?php echo $this->session->userdata('name')?></a></li>
                    <?php } ?>

                  </ul>
              </div>
              <div class="col">
                  <a href="#" class="js-menu-trigger menu dark-menu">
                      <i class="fa fa-bars fa-lg"></i>&nbsp;
                      <span>MENU</span>
                  </a>
              </div>
          </div>
      </div>
  </header>


  <div class="js-expanded-menu expanded-menu-holder clearfix">
      <div class="col col-half js-close-menu-trigger close-menu-area"></div>
      <div class="col col-half expanded-menu">
          <a href="#" class="js-close-menu-trigger close-menu menu">
                  <img src="<?php echo base_url().'assets/meemeep/'?>static/da7JeD0XjB5AciKaLStBwm84M9pIqsXAN6qVNtQofxy.png" alt="Grails">
              CLOSE
          </a>
          <div class="menu-container-top clearfix">
              <nav>
                  <ul class="mega-menu">
                      <li class="no-child">
                          <a href="<?php echo base_url().'importexport'?>">Import/Exports</a>
                      </li>
                      <li class="no-child">
                          <a href="<?php echo base_url().'localtransit'?>">Local transits</a>
                      </li>
                      <li class="no-child">
                          <a href="<?php echo base_url().'claimevent'?>">In the event of a claim</a>
                      </li>
                      <li class="no-child">
                          <a href="<?php echo base_url().'legal'?>">Important Documents</a>
                      </li>
                      <li class="no-child">
                          <a href="<?php echo base_url().'aboutus'?>">About Us</a>
                      </li>

                  </ul>
              </nav>
          </div>
          <div class="cta-menu-holder no-child clearfix">
              <a href="<?php echo base_url().'gotquestion'?>">Got Questions?</a>
              <a href="<?php echo base_url().'contactus'?>" class="js-form-triggerx">Contact Us</a>
              <?php if($this->session->userdata('id') == '') { ?>
                <a href="#" class="js-close-menu-trigger2" data-toggle="modal" data-target="#loginModal" id="login">Sign in</a>
              <?php } else {?>
                <a href="<?php echo base_url().'dashboard' ?>" class="js-close-menu-trigger2"><?php echo $this->session->userdata('name')?></a>
              <?php } ?>
          </div>
      </div>
  </div>
