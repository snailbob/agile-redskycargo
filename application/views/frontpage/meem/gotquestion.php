<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content text-center">Got Questions?</h3>
      </div>
      <div class="row">
         <div class="col-sm-3 col-lg-3 still_menu">
            <ul class="mainmenu gotquestion_menu">
               <li><a href="#answer1" data-target="answer1" class="active">Insurance cover <i class="fa fa-angle-double-right pull-right"></i></a></li>
               <li><a href="#answer2" data-target="answer2">Completing a quote  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
               <li><a href="#answer3" data-target="answer3">Claims  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
               <li><a href="#answer4" data-target="answer4">General insurance questions  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
               <li><a href="#answer5" data-target="answer5">Incoterms  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
               <li><a href="#answer6" data-target="answer6">Buying and selling goods  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-3 answer_container">
            <div id="answer1">
               <p><strong>INSURANCE COVER</strong></p>
               <p><strong><em>What is Transit Insurance?</em></strong><strong></strong></p>
               <p>Transit Insurance, also known  as Cargo Insurance and Marine Cargo Insurance, covers goods for any loss or  damage whilst it is being moved from one location to another. It covers all  modes of transport including road, rail, air and sea.<strong> </strong></p>
               <p><strong><em>What is Domestic Cargo Insurance?</em></strong><strong></strong></p>
               <p>Domestic Cargo Insurance covers  goods that are shipped within one country and are not intended to form any part  of an import or export transaction.</p>
               <p><strong><em>What is an &ldquo;All-Risks&rdquo; Cargo  Insurance Policy?</em></strong><strong></strong></p>
               <p>An All-Risks Cargo Insurance Policy  is the broadest form of cargo insurance available and covers most incidents of physical  loss/damage from an external cause. An all-risks policy will clearly state any  exclusions not covered within the policy. </p>
               <p><strong>Examples of common standard exclusions include:</strong></p>
               <ul>
                  <li>Inherent nature of the goods </li>
                  <li>Insufficient or inadequate packing </li>
                  <li>Ordinary wear and tear </li>
                  <li>Delay, consequential loss, or loss of market </li>
               </ul>
               <p>Institute Cargo Clauses (A)  provides all-risks cargo insurance cover. </p>
               <p><strong><em>Is storage or warehouse cover  included?</em></strong></p>
               <p>The policy covers your goods  whilst temporarily stored during the <strong>ordinary  course of transit</strong>. Cover will cease upon arrival at the final destination. </p>
               <p>If you interrupt or stop the  transit before it reaches its final destination - or choose to store the goods  outside of the normal course of transit - the cover will cease. </p>
               <p>If the cargo is not immediately  unloaded on arrival, but is stored either in the container or in/on the  conveyance, cover ceases at the point prior to unloading.</p>
               <p><strong><em>I&rsquo;m an exporter who usually sends  my goods to a consolidator who packages them into overseas containers for me.  Does the RedSky policy cover the goods during transit, while at the  consolidators, and while being placed into the container?</em></strong></p>
               <p>Yes. Your goods will be covered  for transit to the packers  warehouse and whilst there for a period <strong>not  exceeding 90 days</strong> prior to shipment. </p>
               <p><strong><em>I&rsquo;m an importer and often have the  need to store my goods temporarily before I take delivery of them. Does the  RedSky policy provide coverage automatically for this temporary storage? </em></strong></p>
               <p>Probably not. The policy covers  goods whilst in transit, including during ordinary/customary delays or  interruptions in transit beyond your control. Coverage for any other  interruption must be specifically agreed to and added to this policy.  <br />
                  If you require additional cover  please <u>contact us (link to contact us  page)</u>, or your insurance broker.
               </p>
               <p><strong><em>How much insurance should I  purchase?</em></strong><strong></strong></p>
               <p>In most cases the value of the  goods will be the invoice costs plus freight. An additional percentage (usually  10 per cent of the total) can be added to take into account any other expenses or  charges which may be incurred during shipment.</p>
               <p>If you know you need to insure  for more than cost, insurance and freight plus 10 per cent, <u>contact us</u> to discuss the various options available to you. </p>
               <p><strong><em>My goods are not at risk so why  should I get marine insurance?</em></strong><strong></strong></p>
               <p>It is possible that your goods  may be less prone to loss or damage than others or may be of a low value, but  you still run the risk of a ship sinking, a plane crashing, or some other  catastrophic event occurring. In addition, you are vulnerable to <strong>General Average </strong>losses. </p>
               <p><strong><em>What is General Average?</em></strong><strong></strong></p>
               <p>All goods shipped by sea are  subject to the law of General Average. This is a principle of maritime law  where, in the event of an emergency, voluntary sacrifice is made to safeguard  the vessel, cargo, or crew from a common peril (e.g. jettison of cargo to  extinguish a fire) or expenses are incurred (e.g. a tug is employed to tow a  vessel to safety) and the loss is shared proportionately by all parties with a  financial interest in the voyage.  </p>
               <p>In other words, if you are not  insured against General Average, and there is a loss on the ship your goods are  on, you may incur significant expense to compensate those who were affected,  regardless of whether your goods were directly affected or not.  </p>
               <p><strong><em>How is the cost of cargo insurance  calculated?</em></strong><strong></strong></p>
               <p><em>There  are a number of factors taken into account when calculating the cost of your  insurance, most importantly the:</em></p>
               <ul>
                  <li><em>Susceptibility to damage of the goods being shipped</em></li>
                  <li><em>Country risks of origin and destination</em></li>
                  <li><em>Type of packaging being used</em></li>
                  <li><em>Types of conveyance used</em></li>
                  <li><em>Shipment values</em></li>
                  <li><em>Deductible</em></li>
               </ul>
               <p><strong><em>What is a Certificate of  Insurance?</em></strong></p>
               <p>A Cargo Certificate of  Insurance often accompanies shipment documentation to the consignee (buyer) and  is a negotiable document authorising the bearer to collect a claim settlement. </p>
               <p>Banks will always request a  Certificate of Insurance when a letter of credit is involved in the  transaction. Letters of Credit have specific requirements for the completion of  a Certificate of Insurance that need to be followed closely.</p>
            </div>
            <!-- answer1 -->
            <div id="answer2">
               <p><strong>COMPLETING A QUOTE</strong></p>
               <p><strong><em>What conveyance do I list if my  cargo is going by a combination of transportation modes – e.g. truck to the  port, then by ship overseas, then truck again to the final destination?</em></strong><strong></strong></p>
               <p>In most cases you will simply need  to list the <strong>primary conveyance</strong> used  to move the cargo. The primary conveyance is the main mode of transportation  used to get the cargo from the point of origin to its final destination. On  international shipments, this is usually air or ocean (by ship). However, if a  combination of truck, air and ship is being used to transport your goods you  need to select the &ldquo;Combination&rdquo; key.</p>
               <p><strong><em>What if the HS Code or Cargo  Description is not shown?</em></strong><strong></strong></p>
               <p>In this case simply use the  closest description of your goods you can find. If your goods are your home  contents and personal effects, type in &ldquo;Household&rdquo; and the relevant HS Code  will appear.</p>
               <p><strong><em>What if the HS Code or Cargo  Description is not shown or the goods I am shipping are a variety of different  HS Codes/descriptions </em></strong><strong></strong></p>
               <p>Use the closest description of  your goods you can find. If your goods include many varieties simply use the  description of the goods of the highest value.</p>
               <p><strong><em>What happens if a &ldquo;Referral&rdquo; is  required? </em></strong><strong></strong><br />
                  There are two reasons why the  quotation you have submitted may require &ldquo;Referral&rdquo;:
               </p>
               <ol>
                  <li>The  country you are shipping from or too is restricted or a sanctioned country</li>
                  <li>The  cargo you are shipping may require special insurance conditions or be outside  our ability to insure</li>
               </ol>
               <p>In this instance we will assess  the risk and advise you of your options by email.</p>
               <p><strong><em>What if I don&rsquo;t yet know the exact  number of units to be shipped?</em></strong><strong></strong></p>
               <p>As long as you  know the general description of the goods you want to ship, and their total  value, you can still get a quotation. A full description of the goods can be  provided when the quotation is accepted and before you bind cover.</p>
               <p><strong><em>What if I need to cancel the  policy or make a change to the description, vessel name, etc. on the policy  after I have bound cover?</em></strong><strong></strong></p>
               <p>Simply contact  us and we will cancel or revise your policy as needed. All we need is your policy  certificate number. You can email us <a href="<?php echo base_url().'contactus'?>">here</a> or phone +61 447 668 777.</p>
               <p><strong><em>Can I purchase cargo insurance for  goods shipped anywhere in the world?</em></strong><strong></strong></p>
               <p>Absolutely. The  insurance provider behind RedSky, are syndicates of Lloyds of London, the  world&rsquo;s biggest insurance specialists. </p>
               <p>However, there  are some countries that prohibit shipping to or from their borders, which means  we are legally prohibited from providing cover for any goods shipped to those  countries. There are also certain countries that require imports and exports to  be insured in their local insurance market. </p>
               <p>If one of those  countries is involved in your quotation we will alert you that a &ldquo;Referral&rdquo; is  required.</p>
            </div>
            <!-- answer2 -->
            <div id="answer3">
               <p><strong>CLAIMS</strong></p>
               <p><strong><em>What do I do in the event of a  cargo insurance claim?</em></strong></p>
               <p>In the event of loss or damage to your goods you should immediately  notify the agent shown on your <strong>insurance certificate</strong>. This agent is our  correspondent and will know exactly how to help you. The agent will inspect your  goods to determine the cause of loss or damage, calculate the value of the  damage plus remaining cargo, and survey the extent of the damage. </p>
               <p>You will need to provide the agent with documentation including insurance  certificate, the original invoice, bill of lading and repair invoice if  applicable. </p>
               <p><strong>Important</strong>:  DO NOT under any circumstances destroy or dispose of damaged goods until the agent  has inspected the goods or confirmed to you that they are no longer required.</p>
            </div>
            <!-- answer3 -->
            <div id="answer4">
               <p><strong>GENERAL INSURANCE QUESTIONS</strong></p>
               <p><strong><em>Who/What  is Lloyd&rsquo;s of London?</em></strong></p>
               <p>Serving over 190 countries and  territories, Lloyd&rsquo;s of London is the world&rsquo;s premier marine, truck, rail and  air cargo insurer. Not an insurance company itself, Lloyd&rsquo;s is a a society of  members who underwrite in &ldquo;syndicates&rdquo; on behalf of investment institutions,  specialist investors, international insurance companies and individuals.</p>
               <p><strong><em>Won&rsquo;t my transportation carrier  pay my losses?</em></strong><strong></strong></p>
               <p>No. They <u>will not</u>.  Transportation carriers are not obligated to pay for your losses that occur  beyond their control. They are only responsible for loss or damage when it is  due to their own negligence while the goods are in their care, custody or  control. </p>
               <p>Also, international laws can  limit the liability of ocean carriers and airlines. Similarly trucking  companies, rail carriers and warehouse owners may also be able to limit their  liability. This will depend on local country laws and the conditions under  which they contract with you.</p>
            </div>
            <!-- answer4 -->
            <div id="answer5">
               <p><strong>INCOTERMS</strong></p>
               <p>One of the many  important questions that must be decided in every international trade  transaction is what functions and responsibilities each party to the  transaction will undertake – i.e. which party is obliged to arrange the freight  and insurance and where title and risk will transfer.  </p>
               <p><strong>Incoterms</strong> are a set of internationally recognised  trade terms, which define the obligations and responsibilities of the seller  and buyer for the delivery of goods under the sales contract. They are  published by the International Chamber of Commerce (ICC) and are widely used in  commercial transactions.</p>
               <p>The Incoterms 2010 are organised into two categories:</p>
               <p><strong>Incoterms for any Mode or Modes of  Transport:</strong></p>
               <ul type="disc">
                  <li><strong>EXW</strong>&nbsp;-       Ex Works</li>
                  <li><strong>FCA</strong>&nbsp;-       Free Carrier</li>
                  <li><strong>CPT</strong>&nbsp;-       Carriage Paid To</li>
                  <li><strong>CIP</strong>&nbsp;-       Carriage and Insurance Paid</li>
                  <li><strong>DAT</strong>&nbsp;-       Delivered At Terminal&nbsp;</li>
                  <li><strong>DAP</strong>&nbsp;-       Delivered At Place&nbsp;</li>
                  <li><strong>DDP</strong>&nbsp;-       Delivered Duty Paid</li>
               </ul>
               <p><strong>Incoterms for Sea and Inland Waterway  Transport Only:</strong></p>
               <ul type="disc">
                  <li><strong>FAS</strong>&nbsp;-       Free Alongside Ship</li>
                  <li><strong>FOB</strong>&nbsp;-       Free On Board</li>
                  <li><strong>CFR</strong>&nbsp;-       Cost and Freight</li>
                  <li><strong>CIF</strong>&nbsp;-       Cost, Insurance and Freight</li>
               </ul>
               <p>Choosing the  right terms is important as they will determine whether you are required to  insure the goods.</p>
               <p>A simple guide to  the Incoterms 2010 Rules can be found <a href="https://www.searates.com/reference/incoterms/" target="_blank">here</a>.</p>
            </div>
            <!-- answer5 -->
            <div id="answer6">
               <p><strong>BUYING AND SELLING GOODS</strong></p>
               <p><strong><em>I always sell C&amp;F or buy CIF,  why should I change?</em></strong><strong></strong></p>
               <p>Here are the main issues with  buying on Cost Insurance &amp; Freight (CIF) terms:</p>
               <ul>
                  <li>If you  do have to make a cargo insurance claim you will be dealing with an overseas  insurance company that may not treat you as a valued client. Even if the  language barriers are not a problem, which they often are, getting the company  to take care of your claim in a timely manner can be a challenge.</li>
               </ul>
               <ul>
                  <li>Foreign insuring terms are often inferior to the terms offered here in Australia. </li>
               </ul>
               <ul>
                  <li>In the  event of a General Average, an overseas insurance company may not be in a  position to provide a &ldquo;general average guarantee&rdquo; or the guarantee may not be  acceptable to the shipping company. This means you would have to pay a substantial  deposit before you could take possession of your goods. </li>
               </ul>
               <ul>
                  <li>The insurance will often cease at  the port of arrival and you will need to insure the goods from the port to your  final destination. Buying insurance for this last transit can be difficult.</li>
               </ul>
               <ul>
                  <li>You must be satisfied that the  insurance has been effected and that the insuring terms, valuation, and limits  provided by that insurance is going to meet your needs.</li>
               </ul>
               <p>By purchasing on Free on Board  (FOB), or Cost and Freight (C&amp;F), or similar terms that do not include  insurance, you can control your own insurance and deal with your own insurance  company in the case of a loss. You may also find it simpler and more cost  effective.</p>
               <p>If you sell goods FOB or  C&amp;F, then technically you have title and responsibility for the goods until  they are loaded onto the ship or aircraft. Losses frequently occur in transit  before goods are even loaded onto the ship or aircraft.</p>
            </div>
            <!-- answer6 -->
         </div>
      </div>
   </div>
</div>
