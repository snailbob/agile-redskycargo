<p><strong>Scope</strong></p>
<p>This Privacy Policy  applies to information, including Personal Information CrisisFlo collects  through your access of CrisisFlo&rsquo;s website and the use of CrisisFlo&rsquo;s Products  and Services.</p>
<p>If you have purchased CrisisFlo&rsquo;s  Products and Services as outlined in the Master Services Agreement, you also  agree to this Privacy Policy. The terms &quot;We,&quot; &quot;Us,&quot;  &quot;Our,&quot; or &quot;CrisisFlo&quot; includes CrisisFlo and our  affiliates. It explains how we may collect, use, and disclose information we  obtain through &quot;CrisisFlo&rsquo;s Products and Services.&quot;</p>
<p><strong>CrisisFlo  values your privacy</strong></p>
<p>CrisisFlo considers the  guarding of your privacy as of the utmost importance. This Policy states our  policy on maintaining your privacy and handling your personal information.  Personal information means information about an identifiable individual.</p>
<p>CrisisFlo&rsquo;s Privacy  Policy relates to personal information we handle about our customers, visitors  to our websites and other members of the public.</p>
<p><strong>Reasons  for collecting information</strong></p>
<p>CrisisFlo collects personal  information which is necessary to conduct business with you.<br />
  CrisisFlo and its service  providers may use your personal or business information and information you  have supplied for purposes including to:</p>
<ul>
  <li>Identify you as the User  and to provide better client service to you, your business partners and your  employees;</li>
  <li>Perform authorized  financial transactions with you and your associated service providers;</li>
  <li>Operate our business as  it applies to you;</li>
  <li>Keep you informed about CrisisFlo  products and services and those of relevant business partners;</li>
  <li>Participate in the  process of any third party acquisition or potential acquisition of an interest  in us or our assets</li>
  <li>Protect our lawful  interests.</li>
  <li>Fulfil legal and  regulatory obligations With our third-party service suppliers and vendors;</li>
  <li>If CrisisFlo is merged or  sold to another company</li>
</ul>
<p>We may not be able to do  these things, or those outlined below (under &lsquo;How we use your information&rsquo;),  without your personal information. For example, we may not be able to manage  your product user registration or process transactions where applicable.</p>
<p><strong>How we  use and disclose your information</strong></p>
<p>CrisisFlo uses and  discloses your personal information and the information you supply in many ways  necessary for us to do business together: product promotion, user access to  products, user registration and to provide client support and for secure access  to CrisisFlo products and/or websites.</p>
<p><em>Product promotion</em></p>
<p>CrisisFlo may contact you  about product upgrades and new product releases and to inform you of new  services relevant to your business. A number of laws are in place to limit  direct marketing activity and you may notify us at any time that you do not  want to receive any marketing communications from CrisisFlo.</p>
<p><em>User information</em></p>
<p>When setting up a user  account to access products and services, you must provide contact information  (such as name, email address, phone number and other information as required).  In some case financial information (such as credit-card number and expiration  date if you are using this payment method). CrisisFlo uses this information to  fulfil your order and bill you. If we have trouble processing an order, this  contact information is also used to get in touch with you to resolve the  problem.</p>
<p><em>Transactional Services</em></p>
<p>To facilitate the  provision of the transactional service for which you have applied CrisisFlo is  required to pass information you have provided to its service providers. See  the section &lsquo;Disclosures to third parties&rsquo; below for more on CrisisFlo&rsquo;s  service providers.</p>
<p><em>Customer support</em></p>
<p>User Registration links  your CrisisFlo product to your contact details. This allows our support  consultants to provide you with quality advice relevant to your particular  situation when you call CrisisFlo customer support or lodge a support ticket.  If software or documentation needs to be emailed to you, the consultant has all  the information needed to ensure fast service.</p>
<p>After a person  demonstrates proof that he belongs to a certain organization, we may disclose  to that person the names of that organization&rsquo;s Users where required as part of  the normal course of supplying CrisisFlo&rsquo;s Products and Services. We may also  disclose your Personal Information with your permission.</p>
<p><em>Aggregated data</em></p>
<p>Aggregated data that  contains no information specific to a particular individual or business may be  shared with our business partners, for example, aggregated statistical trends  in a particular industry sector.</p>
<p><em>Disclosure to third  parties</em></p>
<p>We may provide your  personal information to our related bodies corporate and our service providers  who assist us with client contact, archival, auditing, accounting, legal,  business consulting, banking, delivery, data processing, automated  communications, and website or technology services.</p>
<p>Some of the third parties  described above including our service providers may be located in the United  States, Singapore or other countries. You acknowledge that while these parties  will often be subject to other confidentiality or privacy obligations, they may  not in all cases be subject to the specific requirements of Australian or New  Zealand privacy laws.</p>
<p>We may use Personal  Information for service-related purposes. There are some circumstances in which  we may provide it to third parties to allow us to facilitate CrisisFlo&rsquo;s  Services in providing you the ability to use CrisisFlo&rsquo;s product&rsquo;s and  services.</p>
<p>We reserve the right to  disclose your personal information when we believe it is appropriate to comply  with the law, facilitate court proceedings or to protect our rights.</p>
<p><strong>Cookies</strong></p>
<p>We use cookies that enable us to monitor traffic patterns and to  serve you more efficiently. A cookie does not identify you personally but it  does identify your computer. You can set your browser to notify you when you  receive a cookie. This will provide you with an opportunity to either accept or  reject it in each instance.</p>
<p><strong>Google Analytics</strong></p>
<p>CrisisFlo uses Google Analytics to collect information about  visitors to its website. These systems use JavaScript code to help analyses how  users use the site. The information generated about your use of the website  will be transmitted to and stored by Google on servers that maybe located  outside Australia. This information is only accessible and used by CrisisFlo to  improve our website, product and systems.</p>
<p><strong>Security</strong></p>
<p>We take all commercially-reasonable steps to store your personal  information securely.<br />
  Unfortunately, the Internet cannot be guaranteed to be 100%  secure, and we cannot ensure or warrant the security of any information you  provide to us. We do not accept liability for unintentional disclosure.<br />
  Personal information that is held by CrisisFlo is protected by a  range of security measures. For example, CrisisFlo web pages that request  sensitive information may employ encryption technologies such as Secure Sockets  Layer (SSL).</p>
<p>You can confirm that any CrisisFlo  data-entry page uses encryption by checking that:</p>
<ul>
  <li>the page address in the  Web browser's tool bar or status bar begins with https://, or</li>
  <li>the padlock icon in the  web browser's tool bar or status bar is locked.</li>
</ul>
<p>If you are considering  sending us any personal information through unencrypted electronic means (e.g.  standard email), please be aware that the information may be less secure in  transit. We are subject to a number of laws requiring us to protect the  security of personal information once it comes into our possession.</p>
<p>If you are a registered  user of any CrisisFlo website, access to your account may be controlled by a  username and password chosen by you. To prevent unauthorized access, you should  choose a strong password and keep it protected from others.</p>
<p>CrisisFlo is also  committed to protecting your information offline. All of your personal and  business information, not just sensitive information, is subject to access  controls.</p>
<p>By using CrisisFlo&rsquo;s  Services or providing Personal Information to us, you agree that we may  communicate with you electronically regarding security, privacy, and  administrative issues relating to your use of CrisisFlo&rsquo;s Services. If we learn  of a security system's breach, we may attempt to notify you electronically or  sending an email to you.</p>
<p><strong>International  Users</strong></p>
<p>By choosing to visit CrisisFlo&rsquo;s  website or use CrisisFlo&rsquo;s range of Products &amp; Services or otherwise  providing information to us, you agree that any dispute over privacy or this  Privacy Policy will be governed by California law.</p>
<p>If you are visiting from  the European Union or other regions with laws governing data collection and  use, please note that you are agreeing to the transfer of your Personal  Information to Australia and the United States to us. By providing your  Personal Information, you consent to any transfer and processing in accordance  with this Policy.</p>
<p><strong>Information  access and correction</strong></p>
<p>If you have any questions  or concerns relating to this Privacy Policy, please contact on <a href="mailto:info@crisisflo.com">info@crisisflo.com</a>. We will then use all commercially reasonable efforts to  promptly determine if there is a problem and take the necessary corrective  action.</p>
<p>Any changes to this  Privacy Policy that may be made from time to time will be available on CrisisFlo&rsquo;s  website. </p>
<p>Last Modified: January  2014</p>

