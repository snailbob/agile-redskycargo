
        </div><!--/.row-->
    </div>
    <!-- /.container -->

    <div id="footer" class="">
    	<div class="container hidden">
        	<div class="row hidden">
            <div class="col-md-12 col-md-offset-1 addLeftPadding">


            	<div class="col-md-5 col-xs-12">
                	<h3>Recent Blogs</h3>
                </div>

            	<div class="col-md-2 col-xs-12">
                	<h3>Company</h3>
                </div>
            	<div class="col-md-5 col-xs-12">
                	<h3>Location</h3>
                </div>
            </div><!--offset2-->
            </div> <!-- /row -->

            <div id="footer-bottom" class="row">

                <div id="copyright">
                	<ul>
                    	<li class="text">&copy; 2014 Your Company Pty Ltd trading as Transit Insurance</li>
                    </ul>
                	<ul>

                    	<li><a href="#" class="terms_btnx">Terms of Use</a></li>
                    	<li><a href="#" class="privacy_btnx">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /footer -->


    <div id="top-button" class="animated fadeInUp" data-0="display: none" data-100="display: block">
    	<img src="<?php echo base_url()?>assets/frontpage/corporate/images/top-button.png" />
    </div>

	</div>





    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Transit Insurance - Terms of Use</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->

    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

    <!-- Plugin JavaScript -->
  	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
      <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
  	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/summernote/js/summernote.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/dist/cropper.min.js"></script>
    <?php /*?><script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script><?php */?>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>assets/admin/js/admin-script.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/js/admin-validation.js"></script>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>


</body>
</html>
