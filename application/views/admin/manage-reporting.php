<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Manage <?php echo $title ?></h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Manage <?php echo $title ?></li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">

               <div  class="panel-title">
                    <a href="<?php echo base_url().'webmanager/reporting/generate_formpage' ?>" class="btn btn-xs btn-default pull-right">
                        Generate Bordereaux Report
                    </a>

                  <!-- <div class="dropdown pull-right">
                     <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-xs">
                     Generate CSV Report
                     <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="#" class="generate_report_btn" data-title="Generate Agent Channel Report" data-type="agentchannel">Agent Channel</a></li>
                        <li><a href="#" class="generate_report_btn" data-title="Generate Billing Report" data-type="billings">Billing</a></li>
                        <li><a href="#" class="generate_report_btn" data-title="Generate Bordereaux Report" data-type="bordereaux">Bordereaux</a></li>
                     </ul>
                  </div> -->
                  <h4><?php echo $title; ?></h4>
               </div>
            </div>
            <?php if(count($insurances) > 0){ ?>
            <div class="bg-white">
               <table class="table table-hover mydataTb">
                  <thead>
                     <tr>
                        <th class="hidden">sort</th>
                        <th>Date</th>
                        <th>Agent</th>
                        <th>Consignor</th>
                        <th>Transit from</th>
                        <th>Transit to</th>
                        <th>Premium</th>
                        <th>Status</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $count = 1;
                        foreach($insurances as $r=>$value){
                            $insurance = $this->common->the_quote_data($value['id']);

                            $details = unserialize($value['details']);
                            $premium = (isset($details['premium'])) ? $details['premium'] : 0;
                            $premium = number_format($premium, 2, '.', ',');
                            $buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
                            $insurance_details = array();
                            foreach($buy_inputs as $bi=>$bival){
                                $insurance_details[$bival['name']] = $bival['value'];
                            }

                            $single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
                            $cust_name = 'Unknown';
                            if(isset($single_input['first_name']) && isset($single_input['last_name'])){
                                $cust_name = $single_input['first_name'].' '.$single_input['last_name'];
                            }
                            $transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
                            $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';


                            $idate = explode(' ', $insurance_details['shipment_date']);
                            $date_purch = (isset($idate[0])) ? strtotime($idate[0]) : '12333';
                            $date_purch = date('d/m/Y',$date_purch);
                            if($value['date_added'] == '0000-00-00 00:00:00'){
                                $date_purch = 'NA';
                            }
                        ?>
                     <tr>
                        <th class="hidden"><?php echo $count; ?></th>
                        <td><?php echo $date_purch ?></td>
                        <td><?php echo $this->common->customer_name($value['customer_id']) ?></td>
                        <td><?php echo $cust_name ?></td>
                        <td><?php echo $transitfrom; ?></td>
                        <td><?php echo $transitto; ?></td>
                        <td><?php echo $insurance['currency'].$premium; ?></td>
                        <td><?php echo $this->common->quote_status($value['status']); ?></td>
                        <td>
                           <?php /*?><button class="btn btn-default btn-xs btn-block pull-right bind_quote_btn <?php echo ($value['status'] != 'A') ? 'disabled' : ''?>" data-id="<?php echo $value['id'] ?>">
                           <?php echo ($value['status'] == 'P') ? 'Bound' : 'Bind'?>
                           </button><?php */?>
                           <a href="<?php echo base_url().'landing/download_quote/'.$value['id']?>" class="btn btn-default btn-xs pull-right">
                           <i class="fa fa-download"></i> Quote PDF
                           </a>
                        </td>
                     </tr>
                     <?php $count++; } ?>
                  </tbody>
               </table>
            </div>
            <?php
               //loop sorted_schedule
               }
               else { echo '<p class="text-center text-muted" style="padding: 50px;">Nothing to show you.</p>'; }
               ?>
         </div>
      </div>
   </div>
   <!--.row-->
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Generate Report</h4>
         </div>
         <form class="generate_report_form">
            <div class="modal-body">
               <div class="row">
                  <div class="col-sm-6 form-group">
                     <label for="shipment_date">From</label>
                     <input type="text" class="form-control date_from" id="date_from" name="date_from" value="<?php echo date("d-m-Y", strtotime("-1 months")); ?>" />
                     <input type="hidden" class="report-type" name="type" value="" />
                  </div>
                  <div class="col-sm-6 form-group">
                     <label for="date_to">Upto</label>
                     <input type="text" class="form-control date_to" id="date_to" name="date_to" value="<?php echo date("d-m-Y"); ?>" />
                  </div>
               </div>
               <div class="row row-bordereaux hidden">
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Select template</label>
                        <select class="form-control" name="report_template">
                           <option value="" data-name="" data-inputs='[]'>New Template</option>
                           <?php
                              if(count($report_templates) > 0){
                              	foreach($report_templates as $r=>$value){
                              		$tdata = unserialize($value['details']);

                              		echo '<option value="'.$value['id'].'" data-inputs=\''.json_encode($tdata).'\' data-name="'.$value['name'].'">'.$value['name'].'</option>';
                              	}
                              }
                              ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Template Name</label>
                        <input type="text" class="form-control" name="template_name" value="" />
                     </div>
                     <div class="form-group report-attributes">
                        <label>Data attributes to export</label>
                        <div class="row">
                           <?php foreach($report_header as $r=>$value) {?>
                           <div class="col-md-4 col-sm-6">
                              <div class="form-group">
                                 <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <label class="btn btn-default">
                                    <input type="checkbox" name="report_header[]" value="<?php echo $r ?>">
                                    <i class="fa <?php echo $report_icons[$r] ?> fa-2x"></i><br />
                                    <?php echo $value ?>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                        <?php /*?><?php foreach($report_header as $r=>$value) { ?>
                        <div class="checkbox">
                           <label>
                           <input type="checkbox" name="report_header[]" value="<?php echo $r ?>">
                           <?php echo $value ?>
                           </label>
                        </div>
                        <?php } ?>
                        <?php */?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Generate</button>
            </div>
         </form>
      </div>
   </div>
</div>
