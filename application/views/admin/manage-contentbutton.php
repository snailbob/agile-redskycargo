<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Manage <?php echo $title; ?></h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Manage <?php echo $title; ?></li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div  class="panel-title">
                  <h4><?php echo $title; ?></h4>
               </div>
            </div>
            <div class="panel-body">
               <div class="btn-group" data-toggle="buttons">
                  <label class="btn <?php echo ($admin_info[0][$thefield] == 'Y') ? 'btn-primary active' : 'btn-default'?>">
                  <input type="radio" name="home_btn_options" id="option2" autocomplete="off" <?php echo ($admin_info[0][$thefield] == 'Y') ? 'checked="checked"' : ''?> value="Y"> Show
                  </label>
                  <label class="btn <?php echo ($admin_info[0][$thefield] != 'Y') ? 'btn-primary active' : 'btn-default'?>">
                  <input type="radio" name="home_btn_options" id="option3" autocomplete="off" autocomplete="off" <?php echo ($admin_info[0][$thefield] != 'Y') ? 'checked="checked"' : ''?> value="N"> Hide
                  </label>
               </div>
            </div>
            <!--panelbody-->
         </div>
         <!--panel-->
      </div>
   </div>
   <!--.row-->
</div>
