
<section class="">
	<div class="container">
    	<div class="row">
        
        	<div class="col-md-12">
            	<?php if ($this->session->flashdata('ok') != ''){ ?>
                <div class="alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('ok'); ?>
                </div>
                <?php } ?>
            
            	<?php if ($this->session->flashdata('error') != ''){ ?>
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
            
            
            	<p class="text-right <?php if($this->session->userdata('type') != 'studio') { echo 'hidden';} ?>"><a href="#" class="btn btn-primary openmodal_add_btn" data-target="#classModal"><i class="fa fa-plus"></i> New Class</a></p>
            	<div class="panel panel-default bg-shoes">
                

                    <?php
						if(count($classes) > 0) {
					?>
                	<div class="panel-body">
                        <h2 class="my-panel-title"><?php echo $title; ?></h2>
                    </div>
                    <div class="bg-white"><!--bg-white-->
                    <table class="table table-hover mydataTb">
                    	<thead>
                        	<tr>
                            	<th>Class</th>
                            	<th>Date</th>
                            	<th>Time</th>
                            	<th>Instructor(s)</th>
                            	<th>Genres(s)</th>
                            	<th></th>
                            </tr>
                        </thead>
                    	<tbody>
                        	<?php foreach($classes as $r=>$value){
														
								$user_id = $value['student_id'];
								$booking_id = $value['id'];
							
								$student_info = $this->master->getRecords('students', array('id'=>$user_id));
								$schedule_info = $this->master->getRecords('schedules', array('id'=>$value['schedule_id']));
								$class_info = $this->master->getRecords('classes', array('id'=>$value['class_id']));
								
								$inst = $this->common->instructor_array($class_info[0]['instructors']);
								$inst = $this->common->display_array($inst);
		
								$gnrre = $this->common->genre_array($class_info[0]['genres']);
								$gnrre = $this->common->display_array($gnrre);
						
								//format time
								$format_start_time = substr($schedule_info[0]['start'], 11); 
								$format_start_time = date_format(date_create($format_start_time), 'h:i a'); 
						
							
								$format_end_time = substr($schedule_info[0]['end'], 11); 
								$format_end_time = date_format(date_create($format_end_time), 'h:i a'); 
								
								?>
                        	<tr>
                            	<td><?php echo $this->common->class_name($value['class_id']) ?></td>
                            	<td><?php echo substr($schedule_info[0]['start'], 0, 10); ?></td>
                            	<td><?php echo $format_start_time.' - '.$format_end_time ?></td>
                            	<td><?php echo $inst ?></td>
                            	<td><?php echo $gnrre ?></td>
                            	<td>
                                

                                    <div class="dropdown pull-right">
                                      <button class="btn btn-default btn-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        Action
                                        <span class="caret"></span>
                                      </button>
                                    
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                      	
                                        <li><a href="<?php echo base_url().'landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id) ?>" target="_blank">Download Ticket</a></li>
                                      </ul>
                                    </div>                                  
                                
                                
                                </td>
                            </tr>
                            <?php

							 } ?>
                        </tbody>
                    </table>
                    </div><!--bg-white-->
                    <?php } else { echo '<div class="panel-body"><h2 class="my-panel-title">'.$title.'</h2></div>';} ?>
                </div>
            </div>
        </div>
    </div>
</section>


