<div class="panel panel-default">
  <div class="panel-body text-center">
    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="" style="max-width: 220px;">
  </div>
  
<!-- List group -->
  <ul class="list-group">
  	
  	
    <li class="list-group-item"><strong>Name: </strong><br><?php echo $this->common->customer_name($id) ?></li>
    <li class="list-group-item"><strong>Address: </strong><br><?php echo $location ?></li>
    <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $business_name ?></li>
    <li class="list-group-item"><strong>About: </strong><br><?php echo $about ?></li>
  </ul>

</div>