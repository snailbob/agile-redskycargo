       <div class="well well-lg">
            <?php /*?><div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>Elf Cafe</strong>
                        <br>
                        2135 Sunset Blvd
                        <br>
                        Los Angeles, CA 90026
                        <br>
                        <abbr title="Phone">P:</abbr> (213) 484-6829
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <em>Date: 1st November, 2013</em>
                    </p>
                    <p>
                        <em>Receipt #: 34522677W</em>
                    </p>
                </div>
            </div><?php */?>
            <div class="row">
                <div class="text-center">
                    <h4><strong><?php echo $name; ?></strong></h4>
                </div>
                </span>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Details</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><em>Class Date</em></h4></td>
                            <td class="text-right"><?php echo $date?></td>
                        </tr>
                        <tr>
                            <td><em>Class Time</em></h4></td>
                            <td class="text-right"><?php echo $time?></td>
                        </tr>
                        <tr>
                            <td><em>Class Genre(s)</em></h4></td>
                            <td class="text-right"><?php echo $genre?></td>
                        </tr>
                        <tr>
                            <td><em>Class Instructor(s)</em></h4></td>
                            <td class="text-right"><?php echo $inst?></td>
                        </tr>
                        <?php /*?><tr class="hidden">
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right">
                            <p>
                                <strong>Subtotal: </strong>
                            </p>
                            <p>
                                <strong>Tax: </strong>
                            </p></td>
                            <td class="text-center">
                            <p>
                                <strong>$6.94</strong>
                            </p>
                            <p>
                                <strong>$6.94</strong>
                            </p></td>
                        </tr><?php */?>
                        <tr>
                            <td></td>
                            <td class="text-right"><h4><strong>Total Cost: <span class="text-danger">$<?php echo $cost; ?></span></strong></h4></td>
                        </tr>
                    </tbody>
                </table>
                <button
                	type="button"
                    class="btn btn-primary btn-lg btn-block book_schedule_btn"
                    data-id="<?php echo $id; ?>"
                    data-name="<?php echo $name; ?>"
                    data-cost="<?php echo $cost; ?>"
                    data-classid="<?php echo $classid; ?>"
                    data-date="<?php echo $date?>"
                    data-time="<?php echo $time?>"
                    data-genre="<?php echo $genre?>"
                    data-inst="<?php echo $inst?>">
                    Book Now <i class="fa fa-chevron-circle-right"></i>
                </button>
                
            </div>