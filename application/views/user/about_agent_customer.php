<div class="panel panel-default">
  <?php /*?><div class="panel-body text-center hidden">
    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="" style="max-width: 220px;">
  </div><?php */?>
  
<!-- List group -->
  <ul class="list-group">
    <li class="list-group-item"><strong>Name: </strong><br><?php echo $name ?></li>
    <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $details['business_name'] ?></li>
    <li class="list-group-item"><strong>Address: </strong><br><?php echo $details['location'] ?></li>
    <li class="list-group-item"><strong>Email : </strong><br><?php echo $details['email'] ?></li>
    <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $details['calling_code'].$details['calling_digits'] ?></li>
  </ul>

</div>