<div class="panel panel-default">
 <?php /*?> <div class="panel-body text-center">
    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="" style="max-width: 220px;">
  </div><?php */?>
  
<!-- List group -->
  <ul class="list-group">
    <li class="list-group-item"><strong>Name: </strong><br><?php echo $this->common->customer_name($id) ?></li>
    <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $business_name ?></li>
    <li class="list-group-item"><strong>Address: </strong><br><?php echo $location ?></li>
    <li class="list-group-item"><strong>Email: </strong><br><?php echo $email ?></li>
    <li class="list-group-item"><strong>Mobile No.: </strong><br><?php echo $contact_no ?></li>
  </ul>

</div>