
<section class="section section-simple">
	<div class="container">
    	<div class="row">
        
        	<div class="col-md-12">
            	<?php if ($this->session->flashdata('ok') != ''){ ?>
                <div class="alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('ok'); ?>
                </div>
                <?php } ?>
            
            	<?php if ($this->session->flashdata('error') != ''){ ?>
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
            
            
            	<p class="text-right"><a href="#" class="btn btn-primary openmodal_add_btn" data-target="#scheduleModal"><i class="fa fa-plus"></i> New Schedule</a></p>
                
                
               
            
                <?php
//                    if(count($sorted_schedule) > 0) {
//                        foreach($sorted_schedule as $r=>$schedules){
                ?>
                
            	<div class="panel panel-default bg-shoes">
                	<div class="panel-body">
                        <h3 class="my-panel-title"><?php echo $title; ?></h3>
                    </div>
                    
                    
                    <?php if(count($sorted_schedule) > 0){ ?>
                    <div class="bg-white"><!--bg-white-->
                    <table class="table table-hover mydataTb">
                    	<thead>
                        	<tr>
                            	<th>Date</th>
                            	<th>Time</th>
                            	<th>Class</th>
                            	<th>Class Size</th>
                            	<th>Enrolled Student</th>
                            	<th>Instructor(s)</th>
                            	<th width="20%">Genre(s)</th>
                            	<th></th>
                            </tr>
                        </thead>
                    	<tbody>
                        	<?php foreach($sorted_schedule as $r=>$value){ ?>
                        	<tr>
                            	<td title="<?php echo date_format(date_create($value['date']), 'F d, Y - l') ?>"><?php echo $value['date'] ?></td>
                            	<td><?php echo $value['format_start_time'].' - '.$value['format_end_time'] ?></td>
                            	<td><?php echo $this->common->class_name($value['class_id']) ?></td>
                            	<td><?php echo $value['max_student'] ?></td>
                            	<td><?php echo $value['student_count'] ?></td>
                            	<td><?php
									if(count($value['instructors_name']) > 0){
										$count = 1;
										foreach($value['instructors_name'] as $instructors_name){
											echo $instructors_name;
											if($count < count($value['instructors_name'])){
												echo ', ';
											}
											
											$count++;
										}
									}
									else{
										echo 'Not added';
									}
								
								?></td>
                            	<td><?php
									if(count($value['genres_id']) > 0){
										$count = 1;
										foreach($value['genres_id'] as $genres_id){
											echo $this->common->genre_name($genres_id);
											if($count < count($value['genres_id'])){
												echo ', ';
											}
											$count++;
										}
									}
									else{
										echo 'Not added';
									}
								
								?></td>
                            	<td>
                                
                                    <div class="dropdown pull-right">
                                      <button class="btn btn-default btn-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        Action
                                        <span class="caret"></span>
                                      </button>
                                    
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">

                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="schedules">Delete</a></li>
                                      </ul>
                                    </div>                                
                                
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div><!--bg-white-->
                    
                <?php
                  //loop sorted_schedule
                 } // else { echo '<div class="panel-body"><h2 class="my-panel-title">'.$title.'</h2></div>';} ?>
               
                </div><!--panel-->
                
            
                
                
            </div>
           
        </div>
    </div>
</section>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                        
                            <h2>Class Timetable</h2>
                            <hr class="star-primary">
                            
                            <form class="" id="schedule_form">

                                      
                            	<div class="form-group">
                                	<label>Class</label>

                                    <select name="class" class="form-control">
                                    	<option value="" data-max="">Select</option>
                                        <?php 
										if(count($classes) > 0){
											foreach($classes as $r=>$value){
												echo '<option value="'.$value['id'].'" data-max="'.$value['max_students'].'" data-cost="'.$value['cost'].'" data-level="'.$value['level'].'">'.$value['name'].'</option>';
											}
										}
										?>
                                    </select>                      
                          
                          
                                </div>
                                                                        
                            	<div class="form-group">
                                	<label>Class Size</label>
                                    <input type="number" class="form-control" name="max_student" />
                                </div>

                                <div class="form-group">
                                    <label>Class Schedule Type</label><br />
                                    <input type="checkbox" name="recur">
                                </div>

								<div class="weekly_settings" style="display: none;">
                                    <div class="well">

                                      <div class="form-group form-inline">
                                          Recurring for 
                                          <input type="number" name="weekly_recure" class="form-control slim_text_input" value="1">
                                          week(s) on:
                                      </div>
                                      
                                      <div class="form-group">
                                                      
                                            <div class="btn-group" data-toggle="buttons">
                                                <?php foreach($days_of_week as $r=>$value){ ?>
                                                
                                                  <label class="btn btn-default">
                                                    <input type="checkbox" name="days_in_week[]" autocomplete="off" value="<?php echo $r; ?>"> <?php echo substr($value, 0, 3);?>
                                                  </label>
                                              
                                                    
                                                <?php } ?>
                                            
                                            
                                            </div>   

                                        
                                      
                
                                      </div><!--form-group-->
                                      <span class="form-helper text-danger" style="display: none;">Required field.</span>
                                                    
                                	</div><!--well-->
                                </div><!--weekly settings-->
                                  

                                <!-- Text input-->
                                <div class="form-group">
                                    <label>Date</label>  
                        
                                    <div class="input-group" id="datetimepicker3">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <input name="start_day" type="text" placeholder="Start Date" class="form-control">
                                    </div>
                        
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label>Start Time</label>  
                                
                                            <div class="input-group" id="datetimepicker3">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                                <input name="start_time" type="text" placeholder="Start Time" class="form-control timepicker" value="6:00 PM">
                                            </div>
                                
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- Text input-->
                                        <div class="form-group">
                                
                                            <label>End Time</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                                <input name="end_time" type="text" placeholder="End Time" class="form-control timepicker" value="7:00 PM">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                
                                <!-- Text input-->
                                <div class="form-group hidden">
                        
                                    <label>Duration</label>
                                    <select name="duration" class="form-control">
                                        <option value="0">Select</option>
                                        <option value="0">0 Minutes</option>
                                        <option value="5">5 Minutes</option>
                                        <option value="10">10 Minutes</option>
                                        <option value="15">15 Minutes</option>
                                        <option value="30" selected="selected">30 Minutes</option>
                                        <option value="60">1 Hour</option>
                                        <option value="120">2 Hours</option>
                                        <option value="180">3 Hours</option>
                                        <option value="240">4 Hours</option>
                                        <option value="300">5 Hours</option>
                                        <option value="360">6 Hours</option>
                                        <option value="420">7 Hours</option>
                                        <option value="480">8 Hours</option>
                                        <option value="540">9 Hours</option>
                                        <option value="600">10 Hours</option>
                                        <option value="660">11 Hours</option>
                                        <option value="720">0.5 Days</option>
                                        <option value="1440">1 Day</option>
                                        <?php /*?><option value="2880">2 Days</option>
                                        <option value="4320">3 Days</option>
                                        <option value="5760">4 Days</option>
                                        <option value="10080">1 Week</option>
                                        <option value="20160">2 Weeks</option><?php */?>
                                    </select>
                                </div>
                                   
                                                                     
                            	<div class="form-group hidden">
                                	<label>Recurring Schedule</label>

                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="recur" value="">
                                        Recur weekly until end date
                                      </label>
                                    </div>                          
                          
                          
                                </div>
                                
                                                                        
                            	<div class="form-group text-right">
                                	<button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="instructorDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h2>Class Details</h2>
                            <hr class="star-primary">
                            
                            <div class="well"></div>
	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
