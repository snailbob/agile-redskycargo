$(document).ready(function() {
    $('.gotquestion_menu a').on('click', function() {
        var $self = $(this);
        var ans = $self.data('target');
        var hide = $self.data('hide');
        var title = $self.data('title');
        var $answer_container = $('.answer_container');
        $self.closest('ul').find('a').removeClass('active').find('i').addClass('hidden');
        $self.addClass('active');
        $self.find('i').removeClass('hidden');

        if (typeof(hide) !== 'undefined') {
            $answer_container.children('div').addClass('hidden');
            $answer_container.find('#' + ans).removeClass('hidden');
            $('.title-content').html(title);
            $('html, body').animate({
                scrollTop: $answer_container.find('#' + ans).offset().top - 220
            }, 500);

        } else {
            $('html, body').animate({
                scrollTop: $answer_container.find('#' + ans).offset().top - 220
            }, 500);
        }

        return false;
    });

    $('#contact_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-warning');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-warning');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'formsubmits/submit_contact', {
                    data: $(form).serializeArray()
                },
                function(res) {
                    console.log(res);
                    bootbox.alert(res.message, function() {
                        $(form).closest('.modal').modal('hide');
                    });
                    $(form).find('button[type="submit"]').html('Submitted!');
                },
                'json'
            ).error(function(err) {
                console.log(err);
                $(form).find('button[type="submit"]').button('reset');
            });
        }

    });
});
