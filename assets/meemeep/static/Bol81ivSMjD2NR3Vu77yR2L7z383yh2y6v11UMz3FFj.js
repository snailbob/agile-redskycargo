function autoResize(t) {
    try {
        var e;
        document.getElementById && (e = document.getElementById(t).contentWindow.document.body.scrollHeight), document.getElementById(t).height = e + "px"
    } catch (i) {}
}! function() {
    for (var t, e = function() {}, i = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeline", "timelineEnd", "timeStamp", "trace", "warn"], n = i.length, o = window.console = window.console || {}; n--;) t = i[n], o[t] || (o[t] = e)
}(), ! function(t, e, i, n) {
    var o = t(e);
    t.fn.lazyload = function(a) {
        function r() {
            var e = 0;
            l.each(function() {
                var i = t(this);
                if (!c.skip_invisible || i.is(":visible"))
                    if (t.abovethetop(this, c) || t.leftofbegin(this, c));
                    else if (t.belowthefold(this, c) || t.rightoffold(this, c)) {
                    if (++e > c.failure_limit) return !1
                } else i.trigger("appear"), e = 0
            })
        }
        var s, l = this,
            c = {
                threshold: 0,
                failure_limit: 0,
                event: "scroll",
                effect: "show",
                container: e,
                data_attribute: "original",
                skip_invisible: !1,
                appear: null,
                load: null,
                placeholder: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
            };
        return a && (n !== a.failurelimit && (a.failure_limit = a.failurelimit, delete a.failurelimit), n !== a.effectspeed && (a.effect_speed = a.effectspeed, delete a.effectspeed), t.extend(c, a)), s = c.container === n || c.container === e ? o : t(c.container), 0 === c.event.indexOf("scroll") && s.bind(c.event, function() {
            return r()
        }), this.each(function() {
            var e = this,
                i = t(e);
            e.loaded = !1, (i.attr("src") === n || i.attr("src") === !1) && i.is("img") && i.attr("src", c.placeholder), i.one("appear", function() {
                if (!this.loaded) {
                    if (c.appear) {
                        var n = l.length;
                        c.appear.call(e, n, c)
                    }
                    t("<img />").bind("load", function() {
                        var n = i.attr("data-" + c.data_attribute);
                        i.hide(), i.is("img") ? i.attr("src", n) : i.css("background-image", "url('" + n + "')"), i[c.effect](c.effect_speed), e.loaded = !0;
                        var o = t.grep(l, function(t) {
                            return !t.loaded
                        });
                        if (l = t(o), c.load) {
                            var a = l.length;
                            c.load.call(e, a, c)
                        }
                    }).attr("src", i.attr("data-" + c.data_attribute))
                }
            }), 0 !== c.event.indexOf("scroll") && i.bind(c.event, function() {
                e.loaded || i.trigger("appear")
            })
        }), o.bind("resize", function() {
            r()
        }), /(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion) && o.bind("pageshow", function(e) {
            e.originalEvent && e.originalEvent.persisted && l.each(function() {
                t(this).trigger("appear")
            })
        }), t(i).ready(function() {
            r()
        }), this
    }, t.belowthefold = function(i, a) {
        var r;
        return r = a.container === n || a.container === e ? (e.innerHeight ? e.innerHeight : o.height()) + o.scrollTop() : t(a.container).offset().top + t(a.container).height(), r <= t(i).offset().top - a.threshold
    }, t.rightoffold = function(i, a) {
        var r;
        return r = a.container === n || a.container === e ? o.width() + o.scrollLeft() : t(a.container).offset().left + t(a.container).width(), r <= t(i).offset().left - a.threshold
    }, t.abovethetop = function(i, a) {
        var r;
        return r = a.container === n || a.container === e ? o.scrollTop() : t(a.container).offset().top, r >= t(i).offset().top + a.threshold + t(i).height()
    }, t.leftofbegin = function(i, a) {
        var r;
        return r = a.container === n || a.container === e ? o.scrollLeft() : t(a.container).offset().left, r >= t(i).offset().left + a.threshold + t(i).width()
    }, t.inviewport = function(e, i) {
        return !(t.rightoffold(e, i) || t.leftofbegin(e, i) || t.belowthefold(e, i) || t.abovethetop(e, i))
    }, t.extend(t.expr[":"], {
        "below-the-fold": function(e) {
            return t.belowthefold(e, {
                threshold: 0
            })
        },
        "above-the-top": function(e) {
            return !t.belowthefold(e, {
                threshold: 0
            })
        },
        "right-of-screen": function(e) {
            return t.rightoffold(e, {
                threshold: 0
            })
        },
        "left-of-screen": function(e) {
            return !t.rightoffold(e, {
                threshold: 0
            })
        },
        "in-viewport": function(e) {
            return t.inviewport(e, {
                threshold: 0
            })
        },
        "above-the-fold": function(e) {
            return !t.belowthefold(e, {
                threshold: 0
            })
        },
        "right-of-fold": function(e) {
            return t.rightoffold(e, {
                threshold: 0
            })
        },
        "left-of-fold": function(e) {
            return !t.rightoffold(e, {
                threshold: 0
            })
        }
    })
}(jQuery, window, document),
function(t, e) {
    var i = function(t, e, i) {
        var n;
        return function() {
            function o() {
                i || t.apply(a, r), n = null
            }
            var a = this,
                r = arguments;
            n ? clearTimeout(n) : i && t.apply(a, r), n = setTimeout(o, e || 100)
        }
    };
    jQuery.fn[e] = function(t) {
        return t ? this.bind("resize", i(t)) : this.trigger(e)
    }
}(jQuery, "smartresize");