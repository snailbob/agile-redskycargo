function openChatBox() {
    document.getElementById("chat-box").style.display = "inline-block", document.getElementById("chat-box").style.visibility = "visible", document.getElementById("button-container").style.display = "none", 0 == chatWindowOpened && (liveagent.startChatWithWindow("57390000000TPjr", "chat-frame"), chatWindowOpened = !0)
}

function closeChatBox() {
    document.getElementById("chat-box").style.display = "none", document.getElementById("button-container").style.display = "inline-block", document.getElementById("chat-frame").html = ""
}

function openCaseBox() {
    document.getElementById("case-box").style.display = "inline-block", document.getElementById("case-box").style.visibility = "visible", document.getElementById("button-container").style.display = "none"
}

function closeCaseBox() {
    document.getElementById("case-box").style.display = "none", document.getElementById("case-box").style.visibility = "hidden", document.getElementById("button-container").style.display = "inline-block"
}
if ($("#fullpage").length > 0) {
    TweenLite.set(".fixed-img-overlay .overlay-slide", {
        autoAlpha: 0
    });
    var fullPageInit = function() {
        $("#fullpage").fullpage({
            anchors: ["home", "page2", "page3", "page4", "page5", "page6"],
            afterLoad: function(e, t) {
                $(this);
                if (1 != t) {
                    $(".fixed-img-overlay").show();
                    var i = $(".fixed-img-overlay .overlay-slide").eq(t - 2);
                    TweenLite.to(".fixed-img-overlay .overlay-slide", .3, {
                        autoAlpha: 0
                    }), TweenLite.to(i, .3, {
                        autoAlpha: 1,
                        ease: Circ.easeOut
                    }), $(".home-menu-outside").fadeIn()
                } else TweenLite.to(".fixed-img-overlay .overlay-slide", .3, {
                    autoAlpha: 0
                }), $(".home-menu-outside").hide();
                $(window).resize()
            },
            onLeave: function(e, t) {
                $(this);
                1 == t && ($(".fixed-img-overlay").hide(), $(".home-menu-outside").hide())
            }
        })
    };
    fullPageInit(), $(".next-slide").on("click", function() {
        return $.fn.fullpage.moveSectionDown(), !1
    }), $(".js-to-top-slide").on("click", function() {
        return $.fn.fullpage.moveTo(1), !1
    }), $(window).smartresize(function() {
        Modernizr.mq("(max-width: 640px)") && !$("#fullpage").hasClass("fp-destroyed") && ($.fn.fullpage.destroy("all"), $(".fixed-img-overlay").hide(), $(".home-menu-outside").hide()), Modernizr.mq("(min-width: 641px)") && $("#fullpage").hasClass("fp-destroyed") && fullPageInit()
    }).resize()
}
$(".js-to-top-page").on("click", function() {
    return TweenLite.to(window, .7, {
        scrollTo: {
            y: 0
        },
        ease: Circ.easeOut
    }), !1
}), TweenLite.set(".expanded-menu-holder, .expanded-form-holder", {
    display: "block"
}), TweenLite.set(".js-expanded-menu", {
    x: "100%"
}), TweenLite.set(".close-menu-area", {
    autoAlpha: 0
});
var menuOpenTl = new TimelineMax({
    paused: 1
});
menuOpenTl.to(".js-expanded-menu", .3, {
    x: "0%"
}), menuOpenTl.to(".close-menu-area", .3, {
    autoAlpha: .7
}), $(".js-menu-trigger").on("click", function() {
    return menuOpenTl.timeScale(1), menuOpenTl.play(), !1
}), $(".js-close-menu-trigger").on("click", function() {
    return menuOpenTl.timeScale(3), menuOpenTl.reverse(), !1
}), $(".mega-menu li.has-child > a").on("click", function() {
    var e = $(this).parent().find(".menu-lvl-2"),
        t = $(this).closest(".has-child").siblings(".has-child").find(".menu-lvl-2");
    return t.hide(), TweenLite.to($(this).closest(".has-child").siblings(".has-child").find(".arrow-toggle"), .1, {
        rotation: 0
    }), e.toggle(), e.eq(0).is(":visible") ? TweenLite.to($(this).parent().find(".arrow-toggle"), .3, {
        rotation: -180,
        ease: Circ.easeOut
    }) : TweenLite.to($(this).parent().find(".arrow-toggle"), .1, {
        rotation: 0
    }), !1
}), TweenLite.set(".expanded-form-holder", {
    x: "100%"
}), TweenLite.set(".close-form-area", {
    autoAlpha: 0
});
var formOpenTl = new TimelineMax({
    paused: 1
});
formOpenTl.to(".expanded-form-holder", .3, {
    x: "0%"
}), formOpenTl.to(".close-form-area", .3, {
    autoAlpha: .7
}), $(".js-form-trigger").on("click", function() {
    return formOpenTl.timeScale(1), formOpenTl.play(), !1
}), $(".js-close-form-area").on("click", function() {
    formOpenTl.timeScale(3), formOpenTl.reverse()
}), $(window).smartresize(function() {
    Modernizr.mq("(max-width: 768px)") && $(".generic-menu-top").length > 0 ? $(".generic-menu-top").prependTo(".menu-container-top") : $(".generic-menu-top").appendTo($(".js-menu-trigger").parent().prev(".col"))
}).resize(), $(".text-slick-holder").slick({
    fade: !0,
    arrows: !1,
    swipe: !1,
    asNavFor: ".img-slick-holder"
}), $(".img-slick-holder").slick({
    arrows: !1,
    dots: !0,
    speed: 300,
    asNavFor: ".text-slick-holder"
}), $(".text-slick-holder .link").on("click", function() {
    return $(".img-slick-holder").slick("slickNext"), !1
}), $(".img-slick-holder .slick-dots").appendTo(".dots-move-here"), $("#fullpage").length > 0 && $(".menu-lvl-2 a").on("click", function() {
    menuOpenTl.reverse()
});
var iOS = navigator.userAgent.match(/(iPod|iPhone|iPad)/);
iOS && $("body").addClass("ios-safari"), $(function() {
    $("div.lazy").lazyload({
        effect: "fadeIn"
    }), $("img.lazy").lazyload({
        effect: "fadeIn"
    })
}), setTimeout(function() {
    $("#fullpage").hasClass("fp-destroyed") && ($(window).on("hashchange", function() {
        var e = window.location.hash.substring(1),
            t = $(".section[data-anchor=" + e + "]");
        t.length > 0 && TweenLite.to(window, 2, {
            scrollTo: {
                y: t.offset().top
            },
            ease: Circ.easeOut
        })
    }), $(window).trigger("hashchange"))
}, 300), $(".generic-menu-top a").on("click", function() {
    var e = "#" + $(this).prop("href").split("#")[1],
        t = $(e).offset().top - $(".generic-header").height() - 20;
    return $(".menu-container-top .generic-menu-top").length && (menuOpenTl.timeScale(3), menuOpenTl.reverse()), $(e).length > 0 && TweenLite.to(window, .3, {
        scrollTo: {
            y: t
        },
        ease: Circ.easeOut
    }), !1
}), $(".faq .answer").each(function() {
    var e = this,
        t = $(e).height();
    TweenLite.set(e, {
        height: 0
    });
    var i = new TimelineLite({
        paused: !0
    });
    i.to(e, .3, {
        height: t,
        ease: Circ.easeOut
    }), i.fromTo(e, .3, {
        autoAlpha: 0,
        y: 20
    }, {
        autoAlpha: 1,
        y: 0,
        ease: Circ.easeOut
    }, "-=.2"), i.to($(e).prev(".question").find(".expand"), .3, {
        rotation: 90,
        ease: Circ.easeOut
    }, "-=.2"), e.animation = i
}), $(".faq .question a, .faq .expand").on("click", function() {
    var e = $(this).closest(".faq"),
        t = e.find(".answer"),
        i = $(".faq").not(e);
    return i.each(function() {
        $(this).hasClass("active") && $(this).find(".answer")[0].animation.reverse(), $(this).removeClass("active")
    }), e.toggleClass("active"), e.hasClass("active") ? t[0].animation.play() : t[0].animation.reverse(), !1
});
var chatWindowOpened = !1;
$(function() {
    $("#chat-box").draggable(), $("#case-box").draggable()
});
var ua = window.navigator.userAgent,
    edge = ua.indexOf("Edge/");
edge && edge > 0 && parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10) > 11 || $("form.quoteForm select").addClass("notIE11");